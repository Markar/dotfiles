# set -U fish_user_paths $HOME/dotnet $fish_user_paths
# set -U DOTNET_ROOT $HOME/dotnet
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias n='nnn'
alias drag='dragon-drop --all-compact --and-exit'
