"--------------------------------------------------------------------------
" Lua plugins
"--------------------------------------------------------------------------
"
lua << EOF
    require('plugins')
EOF


"--------------------------------------------------------------------------
" General settings
"--------------------------------------------------------------------------

" Copy and paste with Ctrl-C, Ctrl-V
source $VIMRUNTIME/mswin.vim
behave mswin

syntax on

set background=dark
set termguicolors " Better colors

set tabstop=4 softtabstop=4
set shiftwidth=4
set relativenumber
set expandtab
set nu
set smartindent
set undodir=~/.vimundo
set undofile
set ignorecase
set smartcase
set autoread
set conceallevel=0

set cmdheight=2
set updatetime=1000
set shortmess+=c
set signcolumn=yes

set splitright
set splitbelow

set colorcolumn=100 

set clipboard-=autoselect
set completeopt=menu,menuone,noselect

let g:vim_json_conceal=0
let g:move_key_modifier = 'C'

colorscheme gruvbox-material
" colorscheme doom-one

"--------------------------------------------------------------------------
" Key maps
"--------------------------------------------------------------------------

" Leader key
let mapleader = ' ' " Leader key
let maplocalleader = ' ' " Local leader key

" Windows
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>ws :split <CR>
nnoremap <leader>wq :close <CR>
nnoremap <leader>wv :vsplit <CR>

" Closing
nnoremap <leader>q :wqa<CR>

" Telescope
nmap <leader><leader> :Telescope<CR>
nnoremap <leader>f :Telescope current_buffer_fuzzy_find<CR>
nnoremap <leader>F :Telescope live_grep<CR>
nnoremap <leader>o :Telescope find_files<CR>
nnoremap <leader>s :Telescope lsp_document_symbols<CR>
nnoremap <leader>S :Telescope lsp_dynamic_workspace_symbols<CR>
nnoremap <leader>p :SearchSession<CR>

" Other
nnoremap <leader>rr :source $MYVIMRC<CR>
nnoremap <leader>g :Git<CR>
vnoremap p "_dP

nnoremap <C-n> :cnext<CR>
nnoremap <C-m> :cprev<CR>

nmap j gj
nmap k gk


"--------------------------------------------------------------------------
" Plugins
"--------------------------------------------------------------------------

" Enables rainbow parentheses
let g:rainbow_active = 1

" Indent guides
let g:indent_guides_enable_on_vim_startup = 1

"--------------------------------------------------------------------------
" Functions
"--------------------------------------------------------------------------
function! VimEnter()
    if @% == ""
        :lua require('session-lens').search_session()
    endif
endfunction

"--------------------------------------------------------------------------
" Autocmd
"--------------------------------------------------------------------------
autocmd VimEnter * call VimEnter()
" autocmd VimEnter * SearchSession
" autocmd FocusGained * checktime
autocmd FileType gdscript set commentstring=#\ %s
autocmd BufWritePost *.tex silent! execute "AsyncRun pdflatex <afile> >/dev/null 2>&1" | redraw!

" Open quickfix after :grep
augroup quickfix
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l* lwindow
augroup END


" Silent grep
cnoreabbrev <expr> grep  (getcmdtype() ==# ':' && getcmdline() =~# '^grep')  ? 'silent grep'  : 'grep'

" Close quickfix before closing vim
aug QFClose
  au!
  au WinEnter * if winnr('$') == 1 && &buftype == "quickfix"|q|endif
aug END

