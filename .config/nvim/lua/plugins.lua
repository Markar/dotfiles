require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'

    use {
        'rmagatti/auto-session',
        config = function()
            require('auto-session').setup {
                log_level = 'info',
                auto_session_suppress_dirs = {'~/', '~/repos'}
            }
        end
    }

    use {
        'rmagatti/session-lens',
        requires = {'rmagatti/auto-session', 'nvim-telescope/telescope.nvim'},
        config = function()
            require('session-lens').setup({
                -- path_display = { 'shorten' },
            })
        end
    }

    use 'matze/vim-move'

    -- use 'ray-x/lsp_signature.nvim'
    use 'numToStr/Comment.nvim'

    -- use 'neovim/nvim-lspconfig'
    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }

    use { 'neoclide/coc.nvim', branch = 'release' }

    use 'nvim-telescope/telescope.nvim'
    use 'nvim-lua/plenary.nvim'

    use 'Yggdroot/indentLine'

    use 'sainnhe/gruvbox-material'
    -- use {
    --     'NTBBloodbath/doom-one.nvim',
    --     config = function()
    --         require('doom-one').setup({
    --             cursor_coloring = false,
    --             terminal_colors = false,
    --             italic_comments = false,
    --             enable_treesitter = true,
    --             transparent_background = false,
    --             pumblend = {
    --                 enable = true,
    --                 transparency_amount = 20,
    --             },
    --             plugins_integrations = {
    --                 neorg = false,
    --                 barbar = false,
    --                 bufferline = false,
    --                 gitgutter = false,
    --                 gitsigns = true,
    --                 telescope = true,
    --                 neogit = true,
    --                 nvim_tree = false,
    --                 dashboard = false,
    --                 startify = false,
    --                 whichkey = false,
    --                 indent_blankline = true,
    --                 vim_illuminate = false,
    --                 lspsaga = false,
    --             },
    --         })
    --     end,
    -- }

    use 'wellle/targets.vim'
    use 'frazrepo/vim-rainbow'
    use 'jiangmiao/auto-pairs'

    use 'ggandor/lightspeed.nvim'

    use 'mbbill/undotree'
    use 'romainl/vim-cool' -- Disables search after move

    use 'junegunn/fzf'
    use 'junegunn/fzf.vim'

    use 'dyng/ctrlsf.vim'

    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }

    use 'tpope/vim-fugitive'
    use 'mhinz/vim-signify'
    -- use 'tpope/vim-surround'
end)

local servers = { 'gdscript', 'sumneko_lua', 'vimls' }

require('lualine').setup {
    options = {
        theme = 'auto',
        component_separators = { left = '', right = ''},
        section_separators = { left = '', right = ''},
    },
}

-- require('lsp_signature').setup(cfg)
require('Comment').setup()

require('telescope').setup{
    defaults = {
        file_ignore_patterns = {'.png', '.jpg', '.jpeg', '.dll', '.git', 'obj', '.swp'}
    }
}

require('lightspeed').setup {
    jump_to_unique_chars = { safety_timeout = 400 },
}

require("telescope").load_extension("session-lens")

require'nvim-treesitter.configs'.setup {
    ensure_installed = 'gdscript',
    highlight = {
        enable = true,
    }
}

local on_attach = function(client, bufnr)
    require 'lsp_signature'.on_attach()

    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { noremap=true, silent=true }
    vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>K', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>cf', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end

-- local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

-- for _, lsp in pairs(servers) do
--     require('lspconfig')[lsp].setup {
--         on_attach = on_attach,
--         capabilities = capabilities,
--         flags = {
--             debounce_text_changes = 150
--         }
--     }
-- end

-- require('lspconfig').sumneko_lua.setup {
--     settings = {
--         Lua = {
--             diagnostics = {
--                 globals = { 'vim' }
--             }
--         }
--     }
-- }


-- local cmp = require'cmp'

local has_words_before = function()
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

-- cmp.setup({
--     mapping = {
--         ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
--         ['<CR>'] = cmp.mapping.confirm({ select = true }),
--         ["<Tab>"] = cmp.mapping(function(fallback)
--             if cmp.visible() then
--                 cmp.select_next_item()
--             elseif has_words_before() then
--                 cmp.complete()
--             else
--                 fallback()
--             end
--         end, { "i", "s" }),
--         ["<S-Tab>"] = cmp.mapping(function()
--             if cmp.visible() then
--                 cmp.select_prev_item()
--             end
--         end, { "i", "s" }),
--     },
--
--     sources = cmp.config.sources({
--         { name = 'nvim_lsp' }
--     })
-- })
